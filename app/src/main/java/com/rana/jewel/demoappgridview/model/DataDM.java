package com.rana.jewel.demoappgridview.model;

/**
 * Created by JewelPC on 8/24/2017.
 */

public class DataDM {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}