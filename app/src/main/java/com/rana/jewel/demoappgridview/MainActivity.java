package com.rana.jewel.demoappgridview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.rana.jewel.demoappgridview.adapter.RecyclerAdapter;
import com.rana.jewel.demoappgridview.model.DataDM;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerAdapter recyclerAdapter;
    private List<DataDM> movieList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        int numberOfColumns = 2;
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        recyclerAdapter = new RecyclerAdapter(movieList);
        recyclerView.setAdapter(recyclerAdapter);

        DataDM dataDM = new DataDM();
        dataDM.setTitle("Talk to Level-1 Speaker");
        movieList.add(dataDM);

        DataDM dataDM2 = new DataDM();
        dataDM2.setTitle("Talk to Level-2 Speaker");
        movieList.add(dataDM2);

        DataDM dataDM3 = new DataDM();
        dataDM3.setTitle("Topic base Discussion");
        movieList.add(dataDM3);

        DataDM dataDM4 = new DataDM();
        dataDM4.setTitle("Re connect Last Caller");
        movieList.add(dataDM4);

        DataDM dataDM5 = new DataDM();
        dataDM5.setTitle("Vocabulary & Grammar");
        movieList.add(dataDM5);

        DataDM dataDM6 = new DataDM();
        dataDM6.setTitle("Talk only Opposite Gender");
        movieList.add(dataDM6);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
