package com.rana.jewel.demoappgridview.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rana.jewel.demoappgridview.R;
import com.rana.jewel.demoappgridview.model.DataDM;

import java.util.List;

/**
 * Created by Jewel Rana on 3/3/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>{

    List<DataDM> itemList;
    private TextView textView;

    public RecyclerAdapter(List<DataDM> movieDMList) {
        this.itemList = movieDMList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row,parent,false);

        MyViewHolder myViewHolder = new MyViewHolder(view);


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DataDM item = itemList.get(position);

        holder.textView.setText(item.getTitle());
        if (position==1){
            holder.textView.setBackgroundResource(R.color.color1);
        }else if(position==2){
            holder.textView.setBackgroundResource(R.color.color2);
        }else if(position==3){
            holder.textView.setBackgroundResource(R.color.color3);
        }else if(position==4){
            holder.textView.setBackgroundResource(R.color.color4);
        }else if(position==5){
            holder.textView.setBackgroundResource(R.color.color5);
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }


    //........................Holder..........................//

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView textView;

        public MyViewHolder(View itemView) {
            super(itemView);

            textView = (TextView) itemView.findViewById(R.id.textView);
        }
    }
}
